import "./App.css";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import AppContainer from "./hoc/AppContainer";
import Login from "./components/Login/Login";
import Translation from "./components/Translation/Translation"
import NotFound from "./components/NotFound/NotFound";
import Profile from "./components/Profile/Profile";

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <AppContainer>
          <h6>Lost In Translation</h6>
          <hr></hr>
        </AppContainer>
        <Switch>
          <Route path="/" exact component={Login} />
          <Route path="/translation" component={Translation} />
          <Route path="/profile" component={Profile} />
          <Route path="/*" component={NotFound} />
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
