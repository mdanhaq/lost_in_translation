import { Link } from "react-router-dom";
import AppContainer from "../../hoc/AppContainer";

const NotFound = () => {
  return (
    <AppContainer>
    <main>
      <h4>Hey, it seems you are lost.</h4>
      <p>This page does not exist</p>
      <Link to="/">Home</Link>
    </main>
    </AppContainer>    
  );
};
export default NotFound;
