import AppContainer from "../../hoc/AppContainer";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { loginAttemptAction } from "../../store/actions/loginActions";
import { Redirect } from "react-router";

const Login = () => {
  const dispatch = useDispatch();
  const { loginError, loginAttempting } = useSelector(state => state.loginReducer)
  const { loggedIn } = useSelector(state => state.sessionReducer)
  
  const [credentials, setCredentials] = useState({
    name: ""
  });

  const onInputChange = (event) => {
    setCredentials({
      ...credentials,
      [event.target.id]: event.target.value,
    });
  };
  const onFormSubmit = (event) => {
    event.preventDefault(); 
    dispatch(loginAttemptAction(credentials)); 
  };

  return (
      <>
      { loggedIn && <Redirect to="/translation" />}
      { !loggedIn &&
      <AppContainer>
      <form className="mt-3" onSubmit={onFormSubmit}>
        
        

<div class="container">
    <div class="row justify-content-center">
        <div class="col-4">
        <img
              src={require("../../image/Logo.png").default}
              height="100"
              width="100"
              alt=""
            />
        </div>
        <div class="col-4">
            <h1> Lost In Translation</h1>
        <p> Get Started</p>
        </div>
    </div>
</div>

        <div className="mb-3">
          <label htmlFor="username" className="form-label">
            Username
          </label>
          <input
            id="name"
            type="text"
            placeholder="What's your name?"
            className="form-control"
            onChange={onInputChange}
          />
        </div>
        

        <button type="submit" className="btn btn-primary btn-lg">
          Login
        </button>
       </form>
          {loginAttempting && <p>Trying to login..</p>}
          {
            loginError && 
            <div className="alert alert-danger" role="alert">
            <h4>Unsuccessful</h4>
            <p className="mb-0">{loginError}</p>
            </div> 
          }
    </AppContainer>
      }
      </>            
  )
}

export default Login;
