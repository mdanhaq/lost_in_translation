export const LoginAPI = {
  login(credentials) {
    return fetch(
      "http://localhost:8000/users",
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(credentials),
      }
    ).then(async (response) => {
      if (!response.ok) {
        const { error = "An unknown error occurred" } = await response.json();
        throw new Error(error);
      }
      return response.json();
    });
  },
};
